Pour utiliser l'outils Gitlab sur votre propre machine suivez les etapes suivantes :
	
	1- generer une clé SSH en ligne de commande : ssh-keygen -o -f ~/.ssh/id_rsa
	2- copier la clé ssh ( dans le fichier /.ssh/id_rsa.pub )
	
	3- se connecter sur: gricad-gitlab.univ-grenoble-alpes.fr
	4- settings > SSH keys 
	
	5- coller la clé dans le cadre
	6- ajouter la clé.
	
	
	7- aller dans dans le dossier où voulez cloner la forge
	8- lancer la commande : git clone git@gricad-gitlab.univ-grenoble-alpes.fr:derguinm/jeu_de_la_gauffre
	
	9- entrer "yes"
	
desormais vous pouvez ajouter ou modifier des fichiers.
