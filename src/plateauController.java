import java.net.URL;
import java.util.ResourceBundle;

import com.sun.rowset.internal.Row;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;


public class plateauController implements Initializable{
    public char[][] grille;    // Tableau 2D de char
    public int nbclic=0;
//Variables ********************************
    @FXML
    private AnchorPane Controller;

    @FXML
    private Button Restart;

    @FXML
    private Button DeZoomer;

    @FXML
    private Label TailleG;

    @FXML
    private Button Zoomer;

    @FXML
    private Label labelJoueur;

    @FXML
    private Button Quit;

    @FXML
    private GridPane gp;

    @FXML
    private Circle cercle;

    @FXML
    private Label labelWinner;
	
    @FXML
    private AnchorPane Anchorgp;
	//Variables***********************************
	
    
    @Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
    	dessiner_grille();
    }
 
    //**********Boutons**********************
	@FXML
	void Restart(ActionEvent event) throws Exception {
		 Stage stage = (Stage) Restart.getScene().getWindow(); 
		 Pane root = (Pane)FXMLLoader.load(getClass().getResource("/fxml/plateau.fxml")); 
		 Scene scene=new Scene(root);
		 stage.setScene(scene);		
		 stage.show();
	   }
		
	
	@FXML
	void Zoom(ActionEvent event) {
		int n=gp.getColumnConstraints().size();
		if (n < 40) {
			double h=gp.getHeight()/(n+10);
			double w=gp.getWidth()/(n+10);
			for (int i = 0; i < n; i++) {
				gp.getRowConstraints().remove(0);
				gp.getColumnConstraints().remove(0);
			}
			for (int i = 0; i < n+10; i++) {
				ColumnConstraints col=new ColumnConstraints(w);
				RowConstraints row=new RowConstraints(h);
				gp.getColumnConstraints().add(col);
				gp.getRowConstraints().add(row);
			}
			TailleG.setText((n+10)+"x"+(n+10));
			cercle.setRadius(cercle.getRadius()/2);
			dessiner_grille();
		}
	}
	
	@FXML
	void DeZoom(ActionEvent event) {
		int n=gp.getColumnConstraints().size();
		if (n > 10) {
			double h=gp.getHeight()/(n-10);
			double w=gp.getWidth()/(n-10);
			for (int i = 0; i < n; i++) {
				gp.getRowConstraints().remove(0);
				gp.getColumnConstraints().remove(0);
			}
			for (int i = 0; i < n-10; i++) {
				ColumnConstraints col=new ColumnConstraints(w);
				RowConstraints row=new RowConstraints(h);
				gp.getRowConstraints().add(row);
				gp.getColumnConstraints().add(col);
			}
			cercle.setRadius(cercle.getRadius()*2);
			TailleG.setText((n-10)+"x"+(n-10));
			dessiner_grille();
		}
	}
	
	public void Quitter(ActionEvent event) {
		 Stage stage = (Stage) Quit.getScene().getWindow(); 
		    // do what you have to do 
		System.out.println("Fin de la partie");
		 stage.close();
	}
	
	//************Boutons**************************
	
	
	public void dessiner_grille() {
		int n=gp.getRowConstraints().size();             // Nombre de lignes
		grille = new char[n][n];
	       
        // Initialisation
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                grille[i][j]= 'X';
            }
        }
	}
	
	public boolean coupValide(int x, int y)
	{
		if(grille[x][y]=='X')
			return true;
		return false;
	}

	public String joueurTour() {
		if(nbclic % 2==0) {
			return "Player 2 ";
		}else {
			return "Player 1 ";
		}
	}
	
	public void effacer_case(MouseEvent event) {
		int x=(int)(event.getX()/(gp.getWidth()/gp.getRowConstraints().size()));
		int y=(int)(event.getY()/(gp.getHeight()/gp.getRowConstraints().size()));	
		String joueur=this.joueurTour();
		labelJoueur.setText(joueur);
		
		if(x==0 && y==0)
		{
			joueur=joueur+" WIN !";
			labelWinner.setText(joueur);
			labelWinner.setVisible(true);			
		}
				
		if(coupValide(x,y))
		{	
			int n=gp.getRowConstraints().size();
			nbclic++;
			for (int i = x; i < n; i++) {
				for (int j = y; j < n; j++) {
	                grille[i][j]= ' ';
					Pane p= new Pane();
					p.setStyle("-fx-background-color: WHITE");
					gp.add(p, i, j);
				}
			}
		}
	}

}
